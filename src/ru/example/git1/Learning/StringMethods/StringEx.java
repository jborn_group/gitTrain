package ru.example.git1.Learning.StringMethods;

import java.util.Arrays;

public class StringEx {

    public static void main(String[] args) {

    String stsr ="This song deserves far more attention" ;

       // System.out.println(Arrays.toString(stt.toCharArray()));

        int[] ints = new int[] { 1, 2, 3 };
        char[] chars = new char[]{'2','2','2','2'};
          //  char[]chars = new char[0];
       // System.out.println(ints);
        //System.out.println(chars);


     //   на входе: "input string to the method test2", 't'
     //   на выходе: 7

        String str ="input string to the method test2";
                    //tupni gnirts ot eht dohtem 2tset

        // Изнает кол-во разрядов  до и после  запятой
        double d = 858.48;
        String s = Double.toString(d);

        int dot = s.indexOf('.');

        System.out.println(dot + " digits " +
                "before decimal point.");
        System.out.println( (s.length() - dot - 1) +
                " digits after decimal point.");
        String ss ="fff";
        ss = ss.replace('f','3');
        System.out.println(ss);
      //  массив длинной 3 или менее символов - метод должен преобразовать его в число.
        // Пример: ['1', '2', '3'] в --> 123;

       System.out.println(fromString(chars));


    }


    public static  int fromString (final  char[] chars){

        if((chars==null)|| chars.length==0  ){
           return 0;
        } else {
            final StringBuffer str = new StringBuffer();

            for (int i = 0; i <chars.length && i<3; i++) {
                str.append(chars[i]);
                str.trimToSize();
            }

            int c = Integer.valueOf(str.toString());

            return c;
        }

    }

    public  static int fromString2(final  char[] chars){

		if (chars == null || chars.length == 0) return 0;
		final int l ;
        if(chars.length<3){
            l = chars.length;
        }else l = 3;
		  return Integer.valueOf(new String(chars, 0, l));

    }


    public static int fromString3 (final  char[] chars) {

        if (chars == null || chars.length == 0) return 0;
        int result = 0;
        // Решение учителя весьма хитрое, в данном случае.
        // Проще решить без циклов, используя встроенные возможности String
        for (int i = 0; i < 3 && i < chars.length; i++) {
            if (i == 0 && chars[i] == '-') continue;
            final char ch = chars[i];
            final int r = Integer.valueOf(String.valueOf(ch));
            result *= 10;// Просто математика
            result += r;
        }
        //тернарный оператор
        return chars[0] == '-' ? -result : result;
        // END

    }
    public  int fromString4(final char[] chars) {
        // BEGIN
        if (chars == null || chars.length == 0) return 0;
        int result = 0;
        // Решение учителя весьма хитрое, в данном случае.
        // Проще решить без циклов, используя встроенные возможности String
        for (int i = 0; i < 3 && i < chars.length; i++) {
            if (i == 0 && chars[i] == '-') continue;
            final char ch = chars[i];
            final int r = Integer.valueOf(String.valueOf(ch));
            result *= 10;// Просто математика
            result += r;
        }
        //тернарный оператор
        return chars[0] == '-' ? -result : result;

    }

    public static  String reverse2(final String in){

        final  StringBuffer stt = new StringBuffer();

        for (int i = in.length()-1 ; i>=0 ; i--){
            stt.append(in.charAt(i));
        }
        return  stt.toString();
    }


    public  static  int charCount (String str , char c){

        char [] charArray = str.toCharArray();
        int countChars=0;
        for ( char ch:charArray) {

            if (ch == c ){
                countChars++;
            }
        }
        return countChars;
    }

   public static String reverseTokens2(final String in) {
        // BEGIN
        final String[] tok = in.split(" ");

         System.out.println(Arrays.toString(tok));
        final StringBuilder sb = new StringBuilder();

        for (String s : tok) {
            sb.append(reverse2(s));
            sb.append(" ");
        }
        final String temp2 = sb.toString();
        return temp2.trim();
        // END
    }
    public static String reverseTokens (String stt){

        stt=stt+" ";
        char [] tokenSearcharray =  stt.toCharArray();


        char[] beforeToken = new  char[tokenSearcharray.length];
        String reverseToken ="";
        int countArr=0;
        System.out.println(Arrays.toString(tokenSearcharray));
        for (int i=0; i<tokenSearcharray.length; i++){

            countArr++;
            if (tokenSearcharray[i] !=  ' ' ) {

                beforeToken[countArr] = tokenSearcharray[i];

            //    System.out.println(beforeToken[countArr]);

            }else {

               // System.out.println(Arrays.toString(beforeToken));
               countArr=0;

                char [] resetTemple = new  char[beforeToken.length];


                String temp = new String(beforeToken);

                reverseToken +=" "+ reverse(temp.trim());
                beforeToken = resetTemple;

            }

        }
        return  reverseToken.trim();
    }

    public static String reverse (String stt){


        final char [] arr = stt.toCharArray();
        final char [] reverseArr = new char[stt.length()] ;
        int counterRev = 0;
        for (int i = arr.length-1; i>=0; i--){

            reverseArr[counterRev]=arr[i];
            counterRev++;

        }
        String reverseStr = new String(reverseArr);

        return reverseStr;

    }


}

/**
 float a = (Float.parseFloat(args[0]));
 float b = (Float.valueOf(args[1]));

 // do some arithmetic
 System.out.println("a + b = " +
 (a + b));
 System.out.println("a - b = " +
 (a - b));
 System.out.println("a * b = " +
 (a * b));
 System.out.println("a / b = " +
 (a / b));
 System.out.println("a % b = " +
 (a % b));


 **/

/**      String s = "";
 if(011 == 9) s += 4;
 if(0x11 == 17) s += 5;
 Integer I = 12345;
 if(I.intValue() == Integer.valueOf("12345")) s += 6;
 System.out.println( I.intValue()+2);
 System.out.println(Integer.valueOf("12345")+2);
 System.out.println(s);



 int i;
 // Concatenate "i" with an empty string; conversion is handled for you.
 String s1 = "" + i;
 or

 // The valueOf class method.
 String s2 = String.valueOf(i);
 **/
