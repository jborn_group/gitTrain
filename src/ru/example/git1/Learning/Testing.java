package ru.example.git1.Learning;

public class Testing {

    public static void main(String[] args) {

        // mute(1);
        // (n & (n - 1)) != 0 проверяет есть ли едигница в числе
/**
 xor(0,0);
 xor(22,0);
 xor(4,4);
 **/
        /**
         String[][] nums = {

         {null, null, null},
         {null, null, null}

         };
         System.out.println(Arrays.deepToString(nums));
         System.out.println(currentMove(nums));

         for(int[] arr2: array1)
         {
         for(int val: arr2)
         System.out.print(val);
         }

         for(int x : numbers ) {
         if( x == 30 )
         continue;
         System.out.print( x );
         System.out.print("\n");
         }
         **/

        String[][] numbers = {
                {"X", "O", "X"},
                {null, "O", "O"},
                {"X", null, "X"}
        };

/**
        for (int i = 0; i < 3; i++) {

            for (int i2 =0; i2 < 3; i2++) {

                if((i+i2)==numbers.length-1) {

                    System.out.println(numbers[i][i2]);
                }

            }


            //   System.out.println(getWinner(numbers));

        }
**/

        System.out.println(getWinner(numbers));




    }

    public static void xor(int a, int b) {

        a = a ^ b;
        b = a ^ b;
        System.out.println(a);
        System.out.println(b);

    }





    public static String getWinner(String nums[][]) {

        String FIGURE_ONE_PLAYER = "X";
        String FIGURE_TWO_PLAYER = "O";
        short columnFirstCounter = 0;
        short columnSecondCounter = 0;
        short lineFirstCounter = 0;
        short lineSecondCounter = 0;
        short mainDiagFirstCounter = 0;
        short mainDiagSecondCounter = 0;
        short sideDiagFirstCounter = 0;
        short sideDiagSecondCounter = 0;

        int winnerPoint = nums.length;

        String temple;

        for (int i = 0; i < nums.length; i++) {

            for (int i2 = 0; i2 < nums.length; i2++) {

                temple = nums[i][i2];
                lineFirstCounter+= figureCheck(temple, FIGURE_ONE_PLAYER);
                lineSecondCounter += figureCheck(temple, FIGURE_TWO_PLAYER);

                if (winnerConditions(lineFirstCounter, winnerPoint)) {
                    return FIGURE_ONE_PLAYER;
                }

                if (winnerConditions(lineSecondCounter, winnerPoint)) {
                    return FIGURE_TWO_PLAYER;
                }

                temple= nums[i2][i];
                columnFirstCounter+= figureCheck(temple, FIGURE_ONE_PLAYER);
                columnSecondCounter+= figureCheck(temple, FIGURE_TWO_PLAYER);

                if (winnerConditions(columnFirstCounter, winnerPoint)) {
                    return FIGURE_ONE_PLAYER;
                }

                if (winnerConditions(columnSecondCounter, winnerPoint)) {
                    return FIGURE_TWO_PLAYER;
                }

                if(i ==i2){

                    temple = nums[i][i2];

                    mainDiagFirstCounter+= figureCheck(temple, FIGURE_ONE_PLAYER);
                    mainDiagSecondCounter+= figureCheck(temple, FIGURE_TWO_PLAYER);

                    if (winnerConditions(mainDiagFirstCounter, winnerPoint)) {
                        return FIGURE_ONE_PLAYER;
                    }

                    if (winnerConditions(mainDiagSecondCounter, winnerPoint)) {
                        return FIGURE_TWO_PLAYER;
                    }

                }

                if((i+i2)==nums.length-1){

                    temple = nums[i][i2];

                    sideDiagFirstCounter+= figureCheck(temple, FIGURE_ONE_PLAYER);
                    sideDiagSecondCounter+= figureCheck(temple, FIGURE_TWO_PLAYER);

                    if (winnerConditions(sideDiagFirstCounter, winnerPoint)) {
                        return FIGURE_ONE_PLAYER;
                    }

                    if (winnerConditions(sideDiagSecondCounter, winnerPoint)) {
                        return FIGURE_TWO_PLAYER;
                    }


                }


            }

            lineFirstCounter=0;
            lineSecondCounter=0;
            columnFirstCounter=0;
            columnSecondCounter=0;

        }


        return null;
    }


    public static boolean winnerConditions(int count, int winnePoint) {

        if (count == winnePoint) {
            return true;
        }

        return false;
    }

    public static short figureCheck(String temple, String figure) {

        if (temple != null) {

            if (temple.equals(figure)) {
                return 1;
            } else return 0;
        }

        return 0;
    }


    public static void mute(int n) {

        System.out.println((n & (n - 1)) == 0);

    }

    public static String currentMove(String[][] nums) {
        // BEGIN (write your solutionhere)
        String FIRST_PLAYER_FIGURE = "X";
        String SECOND_PLAYER_FIGURE = "0";
        int countFirstFigures = 0;
        int countSecondFigures = 0;

        for (String[] arr2 : nums) {

            for (String val : arr2) {
                if (val != null) {
                    if (val.equals(FIRST_PLAYER_FIGURE)) {
                        countFirstFigures++;
                    }
                    if (val.equals(SECOND_PLAYER_FIGURE)) {
                        countSecondFigures++;
                    }
                }
            }

        }

        if (countFirstFigures < countSecondFigures) {
            return "X";
        } else if (countSecondFigures < countFirstFigures) {
            return "0";
        }

        return "X";

    }
}
