package ru.example.git1.Learning.build;

public class CarBuilder {

    String mark  = "Opel";
    int maxSpeed= 150;

    public CarBuilder buildMark(final String mark){
        this.mark=mark;
        return this;
    }

   public CarBuilder buildSpeed(final int maxSpeed){

        this.maxSpeed=maxSpeed;
        return this;

    }

    public String getMark() {
        return mark;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    Car build(){

        return new Car(this);
      //  Car car = new Car();
        //car.setMark(mark);
       // car.setMaxSpeed(maxSpeed);
    }


    public static void main(String[] args) {
        Car car = new CarBuilder().buildMark("Pego").buildSpeed(200).build();
        Car car1 = new CarBuilder().build();

        System.out.println(car);
    }

}
