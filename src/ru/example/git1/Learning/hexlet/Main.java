package ru.example.git1.Learning.hexlet;

//import java.util.Random;
import ru.example.git1.Learning.hexlet.interfaces.RandomCoordinateGetter;

import ru.example.git1.game.model.Field;
import ru.example.git1.game.model.Figure;
import ru.example.git1.game.model.Point;
import ru.example.git1.game.view.ConsoleView;

public class Main {



    public static void main(String[] args) {
        Field f = new Field();
        Point p1 = new Point(0,0);
        Point p2 = new Point(1,1);
        Point p3 = new Point(2,2);
        f.setFigure(p1, Figure.X);
        f.setFigure(p2, Figure.X);
        f.setFigure(p3, Figure.X);

        ConsoleView c = new ConsoleView();
        c.show(f);

        RandomCoordinateGetter r =new RandomCoordinateGetter();
        System.out.println( r.getMoveCoordinate(f).getX()+ " "+
        r.getMoveCoordinate(f).getY());
       // RandomCoordinateGetter r = new RandomCoordinateGetter();

     // r.getMoveCoordinate(new Field());
    }


}

