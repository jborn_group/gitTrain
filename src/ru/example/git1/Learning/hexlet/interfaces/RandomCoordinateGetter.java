package ru.example.git1.Learning.hexlet.interfaces;

import ru.example.git1.game.model.Field;
import ru.example.git1.game.model.Point;
import java.util.Random;

public class RandomCoordinateGetter implements  ICoordinateGetter{

    @Override
    public Point getMoveCoordinate(final Field field) {

        Point result = getRandomPoint();
        while (field.getFigure(result) != null) {
            result = getRandomPoint();
        }
        return result;
    }


    private int getRandomInt() {
        Random rnd = new Random();
        return rnd.nextInt(3);
    }
    private Point getRandomPoint() {
        return new Point(
                getRandomInt(),
                getRandomInt()
        );
    }

}
