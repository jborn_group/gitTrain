package ru.example.git1.Learning.stepik;

import java.math.BigInteger;

public class Recursion {

    public static void main(String[] args) {

        System.out.println(factorial(3));

    }

    public static BigInteger factorial(int n) {
        if (n == 1) {
            return BigInteger.valueOf(1);
        }

        else {
            return BigInteger.valueOf(n).multiply(factorial(n-1));
        }
    }
}
