package ru.example.git1.car_transfer;



    public class Car {
        static int speed;
        public void showSpeed() {    //выводит значение из хипа по ссылке this
            System.out.println("speed в хипе заданый методом setSpeed - " + this.speed);
        }
        public void setSpeed(int newSpeed) { //принимаем значение 12
            this.speed = newSpeed; //записываем по ссылке в хип
            showSpeed();//показываем
            newSpeed = 8;//записали новое значение в стеке
            System.out.println("переменная newSpeed в фрейме стека - " + newSpeed);
            System.out.println("присваиваем переменной speed новое значение = 3");
            speed = 3; //В стеке нет такой переменной, потому она будет записана в хип!
            System.out.println("Java сама добавила this и записала новое значение speed в хип- " + this.speed);
            setSetSpeed(newSpeed);//рак мозга :) в который передаем значение newSpeed = 8

        }
        public void setSetSpeed(int speed) { //теперь и в стеке есть своя переменная speed !!!
            System.out.println("Значение speed в хипе - " + this.speed);
            speed = speed;//в стеке само себя переприсваивает
            System.out.println("переменная speed в фрейме стека - " + speed);
            System.out.println("присваиваем значение speed из фрейма стека значению speed в хипе!");
            this.speed = speed;//присваиваем значение speed из фрейма значению speed в хипе!
            System.out.println("Значение speed заданое уже методом setSetSpeed - " + this.speed); //показываем speed из хипа.

        }
        public static void main(String... args) {
            Car ta4ka = new Car();

            System.out.println("speed в хипе при создании объекта - " + ta4ka.speed);

            ta4ka.setSpeed(12); //Вызываем конкретную функцию из новосозданого объекта
            //Значение хранится в фрейме в переменной newSpeed
        }
    }

