package ru.example.git1.car_transfer;

import   ru.example.git1.cars.*;


public class Test {

    public static void main(String[] args) {
        CarHacker carHacker = new CarHacker();
        printBefore(carHacker);


        String temp1 = carHacker.car1.toString();
        String temp2 = carHacker.car2.toString();

        carHacker.mixer();

        printAfter(carHacker);
    }

    private static void printBefore(CarHacker carHacker){
        System.out.println("####Before hack####\n"
                + "car1"
                + carHacker.car1.toString()
                +"\n"
                + "car2"
                + carHacker.car2.toString());
    }

    private static void printAfter(CarHacker carHacker){
        System.out.println("####After hack####\n"
                + "car1"
                + carHacker.car1.toString()
                +"\n"
                + "car2"
                + carHacker.car2.toString());
    }
}
