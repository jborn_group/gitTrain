package ru.example.git1.cars;

import java.util.Random;

public class Car2 {

    private static final Random r = new Random();

    private String brand = "Korch";
    private Integer maxSpeed = 80;
    private Integer minSpeed = 0;
    private Integer engineNumber = r.nextInt();
    private String driverName = "mrSpeedy";


    static String getBrand(Car2 car2) {
        return car2.brand;
    }

    static String getDriverName(Car2 car2) {
        return car2.driverName;
    }

    static void setDriverName(Car2 car2, String driverName) {
        car2.driverName = driverName;
    }

    Integer getMaxSpeed() {
        return maxSpeed;
    }

    void setMaxSpeed(Integer maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    Integer getMinSpeed() {
        return minSpeed;
    }

    void setMinSpeed(Integer minSpeed) {
        this.minSpeed = minSpeed;
    }

    Integer getEngineNumber() {
        return engineNumber;
    }

    void setEngineNumber(Integer engineNumber) {
        this.engineNumber = engineNumber;
    }

    void setBrand(String brand) {
        this.brand = brand;
    }

    @Override
    public String toString() {
        return String.format("{brand: %s, maxSpeed: %d, minSpeed: %d, engineNumber: %d, driverName: %s}",
                brand,
                maxSpeed,
                minSpeed,
                engineNumber,
                driverName);
    }
}

