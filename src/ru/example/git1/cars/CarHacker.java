package ru.example.git1.cars;

public class CarHacker {


    public static Car1 car1 = new Car1();
    public static Car2 car2 = new Car2();

    public static void main(String[] args) {

        System.out.println(car1.toString());
        System.out.println(car2.toString());
        mixer();
        System.out.println(car1.toString());
        System.out.println(car2.toString());
;


    }


    // BEGIN (write your solution here)
   static public void mixer() {

        transBrand(car1, car2);

        transMaxSpeed(car1, car2);

        transMinSpeed(car1, car2);

        transEngineNum(car1, car2);

        transDriverName(car1, car2);


        /**
         transfer.setBrand(Car2.getBrand(car2));
         car2.setBrand(car1.getBrand());
         Car1.setBrand(Car2.getBrand(transfer));

         transfer.setMaxSpeed(car2.getMaxSpeed());
         car2.setMaxSpeed(Car1.getMaxSpeed());
         car1.setMaxSpeed(transfer.getMaxSpeed());

         transfer.setMinSpeed(car2.getMinSpeed());
         car2.setMinSpeed(Car1.getMinSpeed());
         car1.setMinSpeed(transfer.getMinSpeed());

         transfer.setEngineNumber(car2.getEngineNumber());
         car2.setEngineNumber(Car1.getEngineNumber());
         car1.setEngineNumber(transfer.getEngineNumber());

         Car2.setDriverName(transfer,Car2.getDriverName(car2));
         Car2.setDriverName(car2,Car1.getDriverName());
         car1.setDriverName(Car2.getDriverName(transfer));
         **/

    }

    public static void transBrand(Car1 car1, Car2 car2) {

        Car2 transfer = new Car2();
        transfer.setBrand(Car2.getBrand(car2));
        car2.setBrand(car1.getBrand());
        Car1.setBrand(Car2.getBrand(transfer));
    }

    public  static void transMaxSpeed(Car1 car1, Car2 car2) {

        Integer auto1 = Car1.getMaxSpeed();
        Integer auto2 = car2.getMaxSpeed();

        auto1 = (auto1 + auto2);
        auto2 = auto1 - auto2;
        auto1 = auto1 - auto2;

        car1.setMaxSpeed(auto1);
        car2.setMaxSpeed(auto2);

        /**
         Car2 transfer = new Car2();
         transfer.setMaxSpeed(car2.getMaxSpeed());
         car2.setMaxSpeed(Car1.getMaxSpeed());
         car1.setMaxSpeed(transfer.getMaxSpeed());
         **/
    }

    public static void transMinSpeed(Car1 car1, Car2 car2) {
        Car2 transfer = new Car2();
        transfer.setMinSpeed(car2.getMinSpeed());
        car2.setMinSpeed(Car1.getMinSpeed());
        car1.setMinSpeed(transfer.getMinSpeed());
    }

    public static void transEngineNum(Car1 car1, Car2 car2) {


        //Car2 transfer = new Car2();
        //transfer.setEngineNumber(car2.getEngineNumber());
        //car2.setEngineNumber(Car1.getEngineNumber());
        //car1.setEngineNumber(transfer.getEngineNumber());

         Integer auto1 = Car1.getEngineNumber()  ;
         Integer auto2 = car2.getEngineNumber();

         auto1 = auto1 + auto2;
         auto2 = auto1 - auto2;
         auto1 = auto1 - auto2;

         car1.setEngineNumber(auto1);
         car2.setEngineNumber(auto2);

    }

    public static void transDriverName(Car1 car1, Car2 car2) {

        Car2 transfer = new Car2();
        Car2.setDriverName(transfer, Car2.getDriverName(car2));
        Car2.setDriverName(car2, Car1.getDriverName());
        car1.setDriverName(Car2.getDriverName(transfer));

    }


    // END


}
