package ru.example.git1.game.Extends;

public class B extends A {
    public static void main(String[] args) {
        /**
         // main method
         A a = new B(); // 2
         B b = new B(); // 3
         **/
        // main method
        //  B b = new A(); // 2   ошибка
        A a = new A(); // 3


        ///Мы можем создавать объект  старого класса через новый класс,  но не наоборот.
    }
}
