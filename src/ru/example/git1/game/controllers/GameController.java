package ru.example.git1.game.controllers;

import ru.example.git1.game.model.*;

public class GameController {

    private static final String DEFAULT_GAME_NAME = "TicTac";
    private static final Board DEFAULT_BOARD = new Board(3);
    private static final Player[] DEFAULT_PLAYERS = {new Player("Player1", Figure.X), new Player("Player2", Figure.O)};


    private final String gameName;
    private final Board board;
    private final Player[] players;

    // BEGIN (write your solution here) no-argument constructor

    GameController(){

        this.gameName = DEFAULT_GAME_NAME;
        this.board = DEFAULT_BOARD;
        this.players = DEFAULT_PLAYERS;
    }
    // END

    // BEGIN (write your solution here) constructor with arguments


    GameController (String gameName , Player[] players, Board board){

        if (gameName == null || gameName.equals("")){
            this.gameName = DEFAULT_GAME_NAME;
        }else  this.gameName= gameName;
        if(players == null || players.equals("")){
            this.players = DEFAULT_PLAYERS;
        }else  this.players = players;

        if (board == null || board.equals("")){
            this.board = DEFAULT_BOARD;
        }else this.board = board;


    }

}
