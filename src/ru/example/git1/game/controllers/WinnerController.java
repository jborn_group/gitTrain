package ru.example.git1.game.controllers;

import ru.example.git1.game.model.*;

public class WinnerController {


    /**
     *  функция  напрямую взаимодействует с массивом в обхлод Point and Field ,что нарушен принцип  доступа к данным  нужно переделать функцию
     *  Исключить повтор кода, РЕАЛИЗОВТАЬ СТЕК!!!
     * @param nums
     * @return
     */
    public static String getWinner(String nums[][]) {

        String FIGURE_ONE_PLAYER = "X";
        String FIGURE_TWO_PLAYER = "O";
        short columnFirstCounter = 0;
        short columnSecondCounter = 0;
        short lineFirstCounter = 0;
        short lineSecondCounter = 0;
        short mainDiagFirstCounter = 0;
        short mainDiagSecondCounter = 0;
        short sideDiagFirstCounter = 0;
        short sideDiagSecondCounter = 0;

        int winnerPoint = nums.length;

        String temple;

        for (int i = 0; i < nums.length; i++) {

            for (int i2 = 0; i2 < nums.length; i2++) {

                temple = nums[i][i2];
                lineFirstCounter+= figureCheck(temple, FIGURE_ONE_PLAYER);
                lineSecondCounter += figureCheck(temple, FIGURE_TWO_PLAYER);

                if (winnerConditions(lineFirstCounter, winnerPoint)) {
                    return FIGURE_ONE_PLAYER;
                }

                if (winnerConditions(lineSecondCounter, winnerPoint)) {
                    return FIGURE_TWO_PLAYER;
                }

                temple= nums[i2][i];
                columnFirstCounter+= figureCheck(temple, FIGURE_ONE_PLAYER);
                columnSecondCounter+= figureCheck(temple, FIGURE_TWO_PLAYER);

                if (winnerConditions(columnFirstCounter, winnerPoint)) {
                    return FIGURE_ONE_PLAYER;
                }

                if (winnerConditions(columnSecondCounter, winnerPoint)) {
                    return FIGURE_TWO_PLAYER;
                }

                if(i ==i2){

                    temple = nums[i][i2];

                    mainDiagFirstCounter+= figureCheck(temple, FIGURE_ONE_PLAYER);
                    mainDiagSecondCounter+= figureCheck(temple, FIGURE_TWO_PLAYER);

                    if (winnerConditions(mainDiagFirstCounter, winnerPoint)) {
                        return FIGURE_ONE_PLAYER;
                    }

                    if (winnerConditions(mainDiagSecondCounter, winnerPoint)) {
                        return FIGURE_TWO_PLAYER;
                    }

                }

                if((i+i2)==nums.length-1){

                    temple = nums[i][i2];

                    sideDiagFirstCounter+= figureCheck(temple, FIGURE_ONE_PLAYER);
                    sideDiagSecondCounter+= figureCheck(temple, FIGURE_TWO_PLAYER);

                    if (winnerConditions(sideDiagFirstCounter, winnerPoint)) {
                        return FIGURE_ONE_PLAYER;
                    }

                    if (winnerConditions(sideDiagSecondCounter, winnerPoint)) {
                        return FIGURE_TWO_PLAYER;
                    }


                }


            }

            lineFirstCounter=0;
            lineSecondCounter=0;
            columnFirstCounter=0;
            columnSecondCounter=0;

        }


        return null;
    }


    public static boolean winnerConditions(int count, int winnePoint) {

        if (count == winnePoint) {
            return true;
        }

        return false;
    }

    public static short figureCheck(String temple, String figure) {

        if (temple != null) {

            if (temple.equals(figure)) {
                return 1;
            } else return 0;
        }

        return 0;
    }
}

   /**

    Этот метод расположен в классе: WinnerController.

        метод должен называться getWinner;
        возвращать String или null, в зависимости от результата работы;
        принимает на вход поле (field).
        Для проверки выиграша вам необходимо проверить совпадение фигурок на воображаемых линиях на игровом поле:

        проверить каждый ряд;
        проверить каждый столбец;
        проверить две диагонали.
        И, если в воображаемой линии все три фигурки одинаковые - то вы нашли победителя (при этом не забудьте проверить ситуацию, когда все три фигурки null).

        Если же победитель не найден(нет трех одинаковых фигурок в линию), то вам необходимо вернуть null. Если победитель найден - возвращаем фигурку "X" или "O" соответственно.

        Задание достаточно трудное. По этому лучше расписать поле с различными комбинациями фигурок на бумаге, а потом уже думать над реализацией кода.

        Добавлен вывод на экран тестируемых полей для наглядности. Ошибки выводит обычно в конце - не стесняйтесь пролистывать вывод компилятора.

        Решить задачу можно как строго для поля 3х3(стандартное), так и для квадратного поля любой рзмерности - по желанию(или, или), для усложнения задачи.

        Подсказки

        Стоит отметить, что входящее поле может иметь разное количество фигурок (заполненость поля), разное их расположение. И соответствие "рядов" и "столбцов" осям x & y никто не гарантирует (поле то квадратное и его можно крутить и переворачивать как угодно).

        И вы можете нарваться на бомбу NullPointerException во многих местах. Помните: у null нельзя взять .equals().
    **/

