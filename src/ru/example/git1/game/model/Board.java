package ru.example.git1.game.model;

public class Board {

        private final Figure[][] figures;

        // BEGIN (write your solution here)
        public Board (int dimension){

            this.figures = new Figure[dimension][dimension];

        }
        // END

        public int getSize() {
            return figures.length;
        }


}
