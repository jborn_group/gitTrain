package ru.example.git1.game.model;

public class Player {

    private final String name;

    private final Figure figure;

    // BEGIN (write your solution here)
    public Player(final String name, final Figure figure){
        this.name = name;
        this.figure = figure;
    }
    // END


    public String getName() {
        return name;
    }

    public Figure getFigure() {
        return figure;
    }

    public static void main(String[] args) {

        Player p = new Player("pidor",Figure.X);

        System.out.println(p.getFigure());




    }

}
